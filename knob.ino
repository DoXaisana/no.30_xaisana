# include <Servo.h>

Servo Motor; 
 
int ReglerWert; 
 
int Position; 

void setup()  
{
  Motor.attach(9); 
}

void loop()  
{
  int ReglerWert = analogRead(A0);   
  Position = map(ReglerWert, 0, 1023, 180, 0);

  Motor.write(Position);
}